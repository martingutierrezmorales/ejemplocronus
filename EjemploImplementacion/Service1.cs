﻿using System;
using System.ServiceProcess;
using System.Threading;
using Cronus.Lib;

namespace EjemploImplementacion
{
    public partial class Service1 : ServiceBase
    {
        private Thread looperThread;
        private bool isRunning = true;
       // private FileWatcher fileWatcher;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            // el Inicio lo que hace realmente es saber iniciar el monitoreo y de ahi cada determinado tiempo creo de cada 5 minutos informa que esta activo
            Escucha.Inicio();
            looperThread = new Thread(Looper);
            looperThread.Start();
        }

        protected override void OnStop()
        {
            //El apagado envia un correo de que alguien apago manualmente el Servicio de Windows
            Escucha.Apagado();
            isRunning = false;
        }

        private void Looper()
        {

            try
            {
                // este es opcional para saber si tu  hilo esta ejecutandose puede que el sistema se este trabando y ver que esta encendido pero no funcionando
                Escucha.Timer();



                //Aqui  mi ciclo

            }
            catch (Exception ex)
            {
                Escucha.SendTelegram("martin.gutierrezm", ex.Message, "brinksbot");
            }

        }

    }
}
